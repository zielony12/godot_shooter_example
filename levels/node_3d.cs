using Godot;
using System;

public partial class node_3d : Node3D {
	public override void _Ready() {
		for(int i = 0; i < 5; i++) {
			Enemy instance;
			
			RandomNumberGenerator random = new RandomNumberGenerator();
			random.Randomize();
			
			instance = (Enemy) Preload.EnemyScene.Instantiate();
			instance.SetElement((Element) random.RandiRange(0, 3));
			
			instance.Position = new Vector3(random.RandfRange(-10, 10f), 2f, random.RandfRange(-10f, 10f));
			AddChild(instance);
		}
	}
}
