using Godot;
using System;

public partial class Entity : CharacterBody3D {
	protected int health;
	
	public virtual bool Hit(int amount) {
		health -= amount;
		if(health <= 0) {
			Die();
			return true;
		}
		return false;
	}
	
	private void Die() {
		QueueFree();
	}
	
	public virtual void KilledEntity(Entity entity) {
		GD.Print(this + " (" + GetType() + ") killed " + entity + " (" + entity.GetType() + ")");
	}
}
