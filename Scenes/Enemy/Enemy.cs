using Godot;
using System;

public enum Element {
	Electro,
	Hydro,
	Pyro,
	Dendro
}

// TODO: Path finding and shooting to player

public partial class Enemy : Entity {
	protected Element element;
	private int shootTimer = 100;
	
	public Element GetElement() {
		return element;
	}

	public void SetElement(Element element) {
		this.element = element;
		
		GetNode<MeshInstance3D>("MeshInstance3D").SetSurfaceOverrideMaterial(0, Preload.EnemyMaterials[(int) element]);
	}
	
	private double gravity = (double) ProjectSettings.GetSetting("physics/3d/default_gravity");
	private RandomNumberGenerator random = new RandomNumberGenerator();
	private Label3D healthLabel;
	private Label3D elementLabel;
	
	public override void _Ready() {
		health = 20;
		healthLabel = GetNode<Label3D>("Label3D1");
		elementLabel = GetNode<Label3D>("Label3D2");
		healthLabel.Text = "Health: " + health.ToString();
		elementLabel.Text = "Element: " + element.ToString();

		Tween tween = GetTree().CreateTween();
	}
	
	public override void _PhysicsProcess(double delta) {
		if(!IsOnFloor())
			Velocity = new Vector3(Velocity.X, Velocity.Y - (float) gravity * (float) delta, Velocity.Z);
		Velocity = new Vector3(Velocity.X + random.RandfRange(-1f, 1f) * 10f * (float) delta, Velocity.Y + (IsOnFloor() ? random.RandfRange(-1f, 1f) * 300 * (float) delta : 0), Velocity.Z + random.RandfRange(-1f, 1f) * 10f * (float) delta);
		if(shootTimer > 0)
			shootTimer--;
		else {
			shootTimer = 100;
			Shoot(new Vector3(random.RandiRange(0, 10), random.RandiRange(0, 10), random.RandiRange(0, 10)));
		}
		
		MoveAndSlide();
	}
	
	public override bool Hit(int amount) {
		if(base.Hit(amount))
			return true;
		
		healthLabel.Text = "Health: " + health.ToString();
		
		return false;
	}
	
	public virtual void Shoot(Vector3 rotation) {
		Node3D b = (Node3D) Preload.BulletScene.Instantiate();
		((Bullet) b).owner = this;
		b.Transform = Transform;
		b.Rotation = rotation;
		((Bullet) b).Velocity = -b.Transform.Basis.Z * 25f;
		GetParent().AddChild(b);
	}
}
