using Godot;
using System;
using System.Collections.Generic;

public partial class Player : Entity {
	private Label healthLabel;
	private Label killsLabel;
	private Camera3D camera3D;
	private int mouseSensitivity = 600;
	private int mouseRelX = 0;
	private int mouseRelY = 0;
	private double speed = 5.0;
	private double jumpVelocity = 4.5;
	private int kills = 0;
	private Variant gravity = ProjectSettings.GetSetting("physics/3d/default_gravity");

	private void Resize() {
		camera3D = GetNode<Camera3D>("Head/Camera3D");
		Vector2 size = GetViewport().GetVisibleRect().Size;
		GetNode<Sprite2D>("GUI/Crosshair").Position = new Vector2(size.X / 2, size.Y / 2);
	}

	public override void _Ready() {
		health = 25;
		
		for(int i = 0; i < 4; i++) {
			new Mana(i, GetNode<ProgressBar>("GUI/ProgressBar" + (i + 1).ToString()));
		}
		
		healthLabel = GetNode<Label>("GUI/Label1");
		killsLabel = GetNode<Label>("GUI/Label2");
		
		healthLabel.Text = "Health: " + health.ToString();
		killsLabel.Text = "Kills: " + kills.ToString();
		
		Input.MouseMode = Input.MouseModeEnum.Captured;
		Node rootNode = GetTree().Root;
		rootNode.Connect("size_changed", new Callable(this, nameof(Resize)));
		Resize();
	}

	public override void _PhysicsProcess(double delta) {
		if(!IsOnFloor())
			Velocity = new Vector3(Velocity.X, Velocity.Y - (float) gravity * (float) delta, Velocity.Z);
		if(Input.IsActionJustPressed("Jump") && IsOnFloor())
			Velocity = new Vector3(0.0f, (float) jumpVelocity, 0.0f);
		if(Input.IsActionJustPressed("Shoot"))
			Shoot();
		Vector2 inputDir = Input.GetVector("moveLeft", "moveRight", "moveUp", "moveDown");
		Vector3 direction = (Transform.Basis * new Vector3(inputDir.X, 0f, inputDir.Y)).Normalized();
		if(direction != Vector3.Zero)
			Velocity = new Vector3(direction.X * (float) speed, Velocity.Y, direction.Z * (float) speed);
		else
			Velocity = new Vector3(MoveToward(Velocity.X, 0f, (float) speed), Velocity.Y, MoveToward(Velocity.Z, 0f, (float) speed));

		MoveAndSlide();
	}
	
	public override void _Input(InputEvent ev) {
		if(ev is InputEventMouseMotion evm) {
			Rotation = new Vector3(Rotation.X, Rotation.Y - evm.Relative.X / mouseSensitivity, Rotation.Z);
			camera3D.Rotation = new Vector3(camera3D.Rotation.X - evm.Relative.Y / mouseSensitivity, camera3D.Rotation.Y, camera3D.Rotation.Z);
			camera3D.RotationDegrees = new Vector3(
				Mathf.Clamp(camera3D.RotationDegrees.X - evm.Relative.Y / mouseSensitivity, -90, 90),
				camera3D.RotationDegrees.Y,
				camera3D.RotationDegrees.Z
			);
			mouseRelX = (int) Mathf.Clamp(evm.Relative.X, -50, 50);
			mouseRelY = (int) Mathf.Clamp(evm.Relative.Y, -50, 50);
		}
	}

	private void Shoot() {
		Node3D b = (Node3D) Preload.BulletScene.Instantiate();
		((Bullet) b).owner = this;
		b.Transform = Transform;
		b.Rotation = new Vector3(camera3D.Rotation.X, b.Rotation.Y, b.Rotation.Z);
		((Bullet) b).Velocity = -b.Transform.Basis.Z * 25f;
		GetParent().AddChild(b);
	}
	
	public override bool Hit(int amount) {
		if(base.Hit(amount))
			return true;
		healthLabel.Text = "Health: " + health.ToString();
		
		return false;
	}

	public override void KilledEntity(Entity entity) {
		base.KilledEntity(entity);
		kills++;
		killsLabel.Text = "Kills: " + kills.ToString();
		if(entity is Enemy e) {
			Mana bar = Mana.bars[(int) e.GetElement()];
			bar.SetValue(bar.GetValue() + 15);
		}
	}

	private float MoveToward(float current, float target, float maxDelta) {
		if (Math.Abs(target - current) <= maxDelta)
			return target;
		else
			return current + Math.Sign(target - current) * maxDelta;
	}
}
