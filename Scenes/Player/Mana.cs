using Godot;
using System;

public class Mana {
	private int id;
	private ProgressBar pb;
	private int value;
	
	public static Mana[] bars = new Mana[4];
	
	public Mana(int id, ProgressBar pb) {
		this.id = id;
		this.pb = pb;
		bars[id] = this;
	}
	
	public void SetValue(int value) {
		if(value > 100)
			this.value = 100;
		else if(value < 0)
			this.value = 0;
		else
			this.value = value;
		pb.Value = this.value;
	}
	
	public int GetValue() {
		return value;
	}
}
