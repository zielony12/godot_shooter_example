using Godot;
using System;

public class Preload {
	public static PackedScene BulletScene = ResourceLoader.Load<PackedScene>("res://Scenes/Bullet/bullet.tscn");
	public static PackedScene HitScene = ResourceLoader.Load<PackedScene>("res://Scenes/Particles/hit.tscn");
	public static PackedScene EnemyScene = ResourceLoader.Load<PackedScene>("res://Scenes/Enemy/enemy.tscn");
	public static Material[] EnemyMaterials = {
		ResourceLoader.Load<Material>("res://Scenes/Enemy/electro.tres"),
		ResourceLoader.Load<Material>("res://Scenes/Enemy/hydro.tres"),
		ResourceLoader.Load<Material>("res://Scenes/Enemy/pyro.tres"),
		ResourceLoader.Load<Material>("res://Scenes/Enemy/dendro.tres")
	};
}
