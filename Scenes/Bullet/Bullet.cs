using Godot;
using System;

public partial class Bullet : Area3D {
	public Vector3 Velocity = Vector3.Zero;
	private static Vector3 g = Vector3.Down * 10f;
	public Node owner;
	
	public override void _PhysicsProcess(double delta) {
		Velocity += g * (float) delta;
		LookAt(Transform.Origin + Velocity.Normalized(), Vector3.Up);
		Transform = new Transform3D(Transform.Basis, Transform.Origin + Velocity * (float) delta);
	}
	
	private void _on_body_entered(Node3D body) {
		if(body == owner)
			return;
		if(body is Entity entity)
			if(entity.Hit(5) && owner is Entity eowner)
				eowner.KilledEntity(entity);
		
		Node3D instance = (Node3D) Preload.HitScene.Instantiate();
		instance.Position = Position;
		GetParent().AddChild(instance);
		
		QueueFree();
	}
}
