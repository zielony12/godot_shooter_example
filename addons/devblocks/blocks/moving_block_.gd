@tool
extends "res://addons/devblocks/blocks/basic_block.gd"
class_name MovingBlock_

enum Axis {X, Y, Z}

var direction: int

func _ready():		
	if target_axis == Axis.X:
		position.x = base_pos
	elif target_axis == Axis.Y:
		position.y = base_pos
	else:
		position.z = base_pos
	
	if base_pos < target_pos:
		direction = 1
	else:
		direction = -1

@export var speed: float = 1:
	set(value):
		speed = value

@export var target_pos: float = 0:
	set(value):
		target_pos = value

@export var base_pos: float = 0:
	set(value):
		base_pos = value

@export var target_axis: Axis = Axis.X:
	set(value):
		target_axis = value

func _physics_process(delta):
	var val = speed * direction * delta
	
	if target_axis == Axis.X && (direction == 1 && position.x <= target_pos || direction == -1 && position.x >= target_pos):
		position.z += val
	else:
		swap_direction()
		
	if target_axis == Axis.Y && (direction == 1 && position.y <= target_pos || direction == -1 && position.y >= target_pos):
		position.y += val
	else:
		swap_direction()
		
	if target_axis == Axis.Z && (direction == 1 && position.z <= target_pos || direction == -1 && position.z >= target_pos):
		position.z += val
	else:
		swap_direction()

func swap_direction() -> void:
	var tmp = base_pos
	base_pos = target_pos
	target_pos = tmp
	direction = -direction
