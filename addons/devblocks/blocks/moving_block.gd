@tool
extends "res://addons/devblocks/blocks/basic_block.gd"
class_name MovingBlock

@export var start: Vector3
@export var duration: float
@export var path: Array

func _ready():
	super._ready()
	position = start
	var tween = get_tree().create_tween().set_process_mode(Tween.TWEEN_PROCESS_PHYSICS)
	tween.set_loops().set_parallel(false)
	for val in path:
		if val == null:
			val = start
		tween.tween_property(self, "position", val, duration / 2)
